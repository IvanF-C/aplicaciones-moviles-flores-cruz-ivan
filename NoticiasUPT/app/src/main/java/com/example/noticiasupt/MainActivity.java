package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private Button BotonInicio;
    TextView registro;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText correo= findViewById(R.id.editTextCorreo);
        final EditText contraseña=findViewById(R.id.editTextContaseña);

        registro = (TextView)findViewById(R.id.textCrearCuenta);
        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambio = new Intent(MainActivity.this,Activity_Registro.class);
                startActivity(cambio);

            }
        });
    }
}
