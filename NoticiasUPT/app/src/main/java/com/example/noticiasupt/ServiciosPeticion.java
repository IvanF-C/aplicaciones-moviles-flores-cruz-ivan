package com.example.noticiasupt;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServiciosPeticion {

    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<RegistroUsuarios> registrarUsuario(@Field("username")String correo, @Field("password")String contraseña);

}
