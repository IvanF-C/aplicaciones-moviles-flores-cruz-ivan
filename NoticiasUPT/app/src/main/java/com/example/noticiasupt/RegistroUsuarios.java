package com.example.noticiasupt;

public class RegistroUsuarios {
    public String estado;
    public  String correo;
    public String contraseña;
    public String detalle;

    public RegistroUsuarios(String correo , String contraseña){
        this.correo=correo;
        this.contraseña=contraseña;

    }
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
}
