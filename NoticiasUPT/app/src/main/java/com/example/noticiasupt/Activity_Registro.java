package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.telecom.Call;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Response;

public class Activity_Registro extends AppCompatActivity {

    public Button registro;
    TextView secion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__registro);
        registro= findViewById(R.id.btnRegistro);
        final EditText correo_usuario=findViewById(R.id.editTextNombreR);
        final EditText contraseña_usuario=findViewById(R.id.editTextContraseñaR);
        final EditText contraseñaR_usuario=findViewById(R.id.editTextContraseñaRR);

        secion = (TextView)findViewById(R.id.textViewSecion);
        secion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cambio = new Intent(Activity_Registro.this,MainActivity.class);
                startActivity(cambio);

            }
        });


        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText correo_usuario=findViewById(R.id.editTextNombreR);
                final EditText contraseña_usuario=findViewById(R.id.editTextContraseñaR);
                final EditText contraseñaR_usuario=findViewById(R.id.editTextContraseñaRR);

                if(correo_usuario.getText().toString()=="")
                {
                    correo_usuario.setSelectAllOnFocus(true);
                    correo_usuario.requestFocus();
                    return;
                }
                    if(contraseña_usuario.getText().toString()=="")
                    {
                        contraseña_usuario.setSelectAllOnFocus(true);
                        contraseña_usuario.requestFocus();
                        return;
                    }
                    if(contraseñaR_usuario.getText().toString()=="")
                    {
                        contraseñaR_usuario.setSelectAllOnFocus(true);
                        contraseñaR_usuario.requestFocus();
                        return;
                    }
                    if (!contraseña_usuario.getText().toString().equals(contraseñaR_usuario.getText().toString()))
                    {
                        Toast.makeText(Activity_Registro.this,"Contraseña no Coinciden",Toast.LENGTH_LONG).show();

                    }

                ServiciosPeticion service= API.getApi(Activity_Registro.this).create(ServiciosPeticion.class);
                Call<RegistroUsuarios> registro =service.registrarUsuario(correo_usuario.getText().toString(),contraseña_usuario.getText().toString());
                registroCall.enqueue(new Callback<RegistroUsuarios>(){
                    @Override
                    public void onResponse(Call<RegistroUsuarios>call, Response<RegistroUsuarios> response){
                        RegistroUsuarios peticion =response.body();
                        if(response.body()==null){
                            Toast.makeText(Activity_Registro.this,"Ocurrio un Error por favor intentelo mas tarde", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(peticion.estado=="true"){
                            startActivity(new Intent(Activity_Registro.this,MainActivity.class));
                            Toast.makeText(Activity_Registro.this,"Los datos se han registrado correctamente",Toast.LENGTH_SHORT).show();

                        }
                        else{
                            Toast.makeText(Activity_Registro.this,peticion.detalle,Toast.LENGTH_LONG).show();
                        }

                    }
                    @Override
                    public void onFailure(Call<RegistroUsuarios>call,Throwable t){
                        Toast.makeText(Activity_Registro.this,"Error",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}